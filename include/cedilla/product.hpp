#pragma once
#include <stdint.h>
#include <vector>

namespace hh { namespace {

template<typename T, typename Fn>
void cartesian_product(const std::vector<T> &x, Fn lambda) {

    std::vector<T> y(x.size());

    for (size_t i = 0; i < x.size(); i++) {
        if (x[i] <= 0) { return; }
    }

    for (;;) {
        lambda(y);
        size_t i = x.size();
        while (i --> 0) {
            y[i]++;
            if (y[i] == x[i]) {
                y[i] = 0;
                if (i == 0) { return; }
            } else {
                break;
            }
        }
    }
}

} }
