#pragma once

#include <utility>
#include "morreale.hpp"
#include "affine.hpp"

extern "C" {
#include "../../cadical/src/ccadical.h"
}

namespace hh {

typedef std::pair<std::vector<int32_t>, std::vector<uint64_t>> proposition;

namespace {

template<typename Fn>
proposition make_proposition_inner(const std::vector<int32_t> &v, Fn lambda) {

    int n = v.size();
    size_t words = (n <= 6) ? 1 : (1ull << (n - 6));
    std::vector<uint64_t> tt(words);

    for (uint32_t i = 0; i < (1u << n); i++) {
        uint64_t b = (lambda(i) ? 1 : 0);
        tt[i >> 6] |= (b << (i & 63));
    }

    return proposition(v, tt);
}

template<typename Fn>
proposition make_proposition(const std::vector<int32_t> &v, Fn lambda) {

    std::vector<size_t> idxs;
    std::vector<int32_t> w;

    uint64_t q = 0;

    for (size_t i = 0; i < v.size(); i++) {
        if (v[i] == 1) {
            continue;
        } else if (v[i] == -1) {
            q |= (1ull << i);
        } else {
            w.push_back(v[i]);
            idxs.push_back(i);
        }
    }

    if (w.size() == v.size()) {
        // no constant literals:
        return make_proposition_inner(v, lambda);
    }

    auto lambda2 = [&](uint64_t x) {
        uint64_t p = q;
        for (size_t j = 0; j < idxs.size(); j++) {
            if ((x >> j) & 1) { p |= (1ull << idxs[j]); }
        }
        return lambda(p);
    };

    return make_proposition_inner(w, lambda2);
}


bool is_bit_zero(uint64_t i, const std::vector<uint64_t> &tt) {

    return !(1 & (tt[i >> 6] >> (i & 63)));

}


struct cnf_holder {

    int32_t n_variables;
    int32_t n_clauses;
    std::vector<int32_t> cnf;
    std::vector<int32_t> importants;
    std::vector<int32_t> uniques;
    std::vector<int32_t> assumptions;

    void assume(int32_t lit) { assumptions.push_back(lit); }

    template<typename Fn>
    int multisolve(Fn lambda) {

        int res = 0;
        auto solver = ccadical_init();

        for (auto &&x : cnf) { ccadical_add(solver, x); }

        uint64_t cnf_consumed = cnf.size();

        do {
            size_t n_assumpts = assumptions.size();
            for (auto &&x : assumptions) {
                ccadical_assume(solver, x);
            }
            assumptions.clear();

            res = ccadical_solve(solver);
            if (res == 10) {
                std::vector<int32_t> solution;
                for (auto &&x : importants) {
                    int32_t v = ccadical_val(solver, x);
                    solution.push_back(v);
                }

                // extract the values of variables that must change:
                std::vector<int32_t> usolution;
                for (auto &&x : uniques) {
                    int32_t v = ccadical_val(solver, x);
                    usolution.push_back(v);
                }

                // impose the clause:
                for (auto &&v : usolution) {
                    ccadical_add(solver, -v);
                }
                ccadical_add(solver, 0);
                lambda(solution);
            } else if (n_assumpts > 0) {
                std::vector<int32_t> solution;
                lambda(solution);
                res = 10;
            }

            // add any new clauses that arrived during the evaluation of lambda:
            for (uint64_t i = cnf_consumed; i < cnf.size(); i++) {
                ccadical_add(solver, cnf[i]);
            }
            cnf_consumed = cnf.size();
        } while (res == 10);

        ccadical_release(solver);
        return res;
    }

    void add_clause(const std::vector<int32_t> &v) {
        for (auto&& x : v) { cnf.push_back(x); }
        cnf.push_back(0);
        n_clauses += 1;
    }

    // The totalizer of Bailleux and Boufkhad
    void merge_totalizer(int32_t *x, size_t nx, size_t ny) {

        int32_t* y = x + nx;
        size_t nz = nx + ny;
        std::vector<int32_t> z(nz);
        for (size_t k = 0; k < nz; k++) { z[k] = new_literal(); }

        for (size_t i = 0; i < nx; i++) {
            add_clause({x[i], -z[i]});
            add_clause({-x[i], z[i + ny]});
        }
        for (size_t j = 0; j < ny; j++) {
            add_clause({y[j], -z[j]});
            add_clause({-y[j], z[j + nx]});
        }
        for (size_t i = 0; i < nx; i++) {
            for (size_t j = 0; j < ny; j++) {
                add_clause({x[i], y[j], -z[i+j+1]});
                add_clause({-x[i], -y[j], z[i+j]});
            }
        }
        for (size_t k = 1; k < nz; k++) {
            add_clause({-z[k-1], z[k]});
        }
        for (size_t i = 0; i < nz; i++) {
            x[i] = z[i];
        }
    }

    void sort_segment(int32_t *x, size_t length) {

        if (length >= 2) {
            size_t nx = length >> 1;
            size_t ny = length - nx;
            sort_segment(x, nx);
            sort_segment(x + nx, ny);
            merge_totalizer(x, nx, ny);
        }
    }

    std::vector<int32_t> sort_booleans(const std::vector<int32_t> &v) {
        std::vector<int32_t> u = v;
        sort_segment(&(u[0]), u.size());
        return u;
    }

    /**
     * Adds a proposition specified by a truth table.
     */
    void add_proposition_tt(const proposition &prop) {

        std::vector<uint32_t> blockers;
        int n = prop.first.size();
        for (uint32_t i = 0; i < (1u << n); i++) {
            if (is_bit_zero(i, prop.second)) { blockers.push_back(i); }
        }

        prime_implicants(&(blockers[0]), blockers.size(), n, [&](uint32_t a, uint32_t b) {
            std::vector<int32_t> clause;
            for (int j = 0; j < n; j++) {
                if ((a >> j) & 1) { continue; }
                if ((b >> j) & 1) {
                    clause.push_back(-prop.first[j]);
                } else {
                    clause.push_back(prop.first[j]);
                }
            }
            add_clause(clause);
        });
    }

    /**
     * Converts a cardinality constraint into clauses.
     */
    void constrain_cardinality_tt(const std::vector<int32_t> &v, const std::vector<uint64_t> &tt) {

        size_t n = v.size();

        if (n <= 9) {
            add_proposition_tt(make_proposition(v, [&](uint32_t x){
                return ((tt[0] >> __builtin_popcount(x)) & 1);
            }));
        } else {
            std::vector<int32_t> u = sort_booleans(v);
            for (size_t trues = 0; trues <= n; trues++) {
                if (is_bit_zero(trues, tt)) {
                    size_t falses = n - trues;
                    std::vector<int32_t> clause;
                    if (falses > 0) { clause.push_back(u[falses - 1]); }
                    if (falses < n) { clause.push_back(   -u[falses]); }
                    add_clause(clause);
                }
            }
        }
    }

    void flat_linear_clause(const std::vector<int32_t> &v) {
        for (size_t i = 0; i < (1ull << v.size()); i++) {
            if (__builtin_popcountll(i) & 1) {
                std::vector<int32_t> w = v;
                for (size_t j = 0; j < v.size(); j++) {
                    if ((i >> j) & 1) { w[j] = -w[j]; }
                }
                add_clause(w);
            }
        }
    }

    void linear_clause(const affine_clause& lc) {

        std::vector<int32_t> x = lc.first;

        if (lc.second == -1) {
            if (x.size() > 0) {
                x[0] *= -1;
            } else {
                x.push_back(-1);
            }
        }

        if (x.size() <= 5) {
            flat_linear_clause(x);
        } else {
            // Using a ring buffer, implement a queue in which we pop two
            // elements, XOR them together, and push the result to the back
            // of the queue. When we get to the last three elements, impose
            // a ternary linear clause. We use a queue rather than a stack
            // because it makes the diameter of the 3-regular tree of XOR
            // operations optimal (logarithmic in the size of the clause,
            // rather than linear).

            uint64_t n = x.size();
            uint64_t hare = 0;
            uint64_t tortoise = n;

            while (tortoise > 3 + hare) {
                auto v1 = new_literal();
                auto v2 = x[(hare++) % n];
                auto v3 = x[(hare++) % n];
                flat_linear_clause({v1, v2, v3});
                x[(tortoise++) % n] = v1;
            }

            {
                auto v1 = x[(hare++) % n];
                auto v2 = x[(hare++) % n];
                auto v3 = x[(hare++) % n];
                flat_linear_clause({v1, v2, v3});
            }
        }
    }

    /**
     * Creates and returns a new literal.
     *
     * The first argument specifies whether this is an 'important' variable
     * (one that's part of the solution) or a 'coding' variable (one that's
     * only used to help with the encoding into conjunctive normal form).
     */
    int32_t new_literal(bool important = false, bool unique = true) {
        int32_t lit = (++n_variables);
        if (important) {
            importants.push_back(lit);
            if (unique) {
                uniques.push_back(lit);
            }
        }
        return lit;
    }

    cnf_holder() : n_variables(0), n_clauses(0) {

        // this ensures that the literal 1 is false and -1 is true
        add_clause({-new_literal()});
    }

};


struct cedilla {

    cnf_holder cnf;
    bool inconsistent;
    std::vector<proposition> propositions;
    std::vector<proposition> cardinality_constraints;
    std::vector<affine_clause> affine_irredundant;
    std::vector<affine_clause> affine_redundant;

    cedilla() : cnf(), inconsistent(false) { }

    int32_t new_literal(bool important = false, bool unique = true) {
        return cnf.new_literal(important, unique);
    }

    void add_clause(const std::vector<int32_t> &v) {
        cnf.add_clause(v);
    }

    template<typename Fn>
    void add_proposition(const std::vector<int32_t> &v, Fn lambda) {
        proposition prop = make_proposition(v, lambda);
        propositions.push_back(prop);
    }

    void flat_linear_clause(const std::vector<int32_t> &v) {
        affine_clause ac = simplify_linear_clause(v);
        if (ac.first.size() == 0) {
            if (ac.second == -1) { inconsistent = true; }
            return;
        }
        affine_irredundant.push_back(ac);
    }

    void identify_vars(int32_t x, int32_t y) {
        flat_linear_clause({x, y});
    }

    template<typename Fn>
    void constrain_cardinality(const std::vector<int32_t> &v, Fn lambda) {

        size_t n = v.size();
        std::vector<uint64_t> tt((n + 64) >> 6);

        uint64_t counters[4] = {0, 0, 0, 0};

        for (size_t i = 0; i <= n; i++) {
            uint64_t b = (lambda(n-i, i) ? 1 : 0);
            tt[i >> 6] |= (b << (i & 63));
            counters[b*2+(i&1)] += 1;
        }

        if ((counters[0] == 0) && (counters[1] == 0)) {
            // literally no information:
            return;
        }

        if ((counters[2] == 0) && (counters[3] == 0)) {
            inconsistent = true;
            return;
        }

        bool this_implies_linear = (counters[2] == 0) || (counters[3] == 0);
        bool linear_implies_this = (counters[0] == 0) || (counters[1] == 0);

        if (this_implies_linear) {
            int32_t parity = (counters[3] == 0) ? 1 : -1;
            affine_clause ac = simplify_linear_clause(v, parity);
            if (linear_implies_this) {
                affine_irredundant.push_back(ac);
            } else {
                affine_redundant.push_back(ac);
                cardinality_constraints.emplace_back(v, tt);
            }
        }
    }

    std::vector<int32_t>& compile_cnf() {

        // encode into CNF:
        for (auto&& prop : propositions) { cnf.add_proposition_tt(prop); }
        for (auto&& card : cardinality_constraints) {
            cnf.constrain_cardinality_tt(card.first, card.second);
        }
        for (auto&& lc : affine_irredundant) { cnf.linear_clause(lc); }

        return cnf.cnf;
    }

    void linear_simplify() {
        std::set<affine_clause> original;
        for (auto&& x : affine_redundant) { original.insert(x); }
        for (auto&& x : affine_irredundant) { original.insert(x); }
        int32_t kdim = linear_solve(original, affine_irredundant);
        if (kdim < 0) { inconsistent = true; }
    }

    template<typename Fn>
    int multisolve(Fn lambda) {

        if (inconsistent) { return 20; }

        // perform linear simplification:
        linear_simplify();

        if (inconsistent) { return 21; }

        // invoke SAT solver:
        compile_cnf();
        return cnf.multisolve(lambda);

    }
    
};


} // anonymous namespace

}
