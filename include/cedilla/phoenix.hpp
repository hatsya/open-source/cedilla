#pragma once

#include <iostream>
#include "solver.hpp"

namespace {

struct Phoenix {

    int height;
    int width;
    int gens;
    int ci;
    int cj;
    int dbheight;
    int dbwidth;

    hh::cedilla ced;
    std::vector<std::vector<std::vector<int32_t>>> literals;
    std::vector<bool> is_good;

    Phoenix(int height, int width, int gens, int g1 = 0, int border = 0, bool unique = false) :
        height(height), width(width), gens(gens),
        ci((height - 1) >> 1), cj((width - 1) >> 1),
        dbheight(height - 2 * border), dbwidth(width - 2 * border) {

        // These are the neighbourhoods which can appear in a phoenix
        // see https://conwaylife.com/wiki/User_talk:Prime_Raptor21/Allowed_periodic_phoenix_transitions
        std::vector<int> good_nhoods = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 13, 16, 17, 18, 20, 24, 32, 33, 34, 36, 37, 40, 41, 44, 45, 48, 64, 65, 66, 67, 68, 69, 70, 72, 76, 80, 96, 97, 98, 100, 101, 104, 108, 128, 129, 130, 131, 132, 133, 134, 136, 140, 144, 160, 161, 192, 193, 194, 195, 196, 197, 198, 256, 257, 258, 259, 260, 261, 262, 264, 265, 266, 268, 269, 272, 288, 289, 296, 297, 320, 321, 322, 323, 324, 325, 326, 328, 332, 352, 353, 360, 384, 385, 386, 387, 388, 389, 390};

        // Convert this set to a dense truth table:
        is_good.resize(512);
        for (const auto& x : good_nhoods) { is_good[x] = true; }

        // Create a grid:
        for (int i = 0; i < height; i++) {
            literals.emplace_back(width);
        }

        // Create a literal for each spacetime position:
        for (int k = 0; k < gens; k++) {
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    bool important = (k == g1) && (i >= border) && (i < height - border) && (j >= border) && (j < width - border);
                    bool very_important = important && unique && (i + j >= ci + cj - 2);
                    literals[i][j].push_back(ced.new_literal(important, very_important));
                }
            }
        }
    }

    void impose_sparsity(bool halfplane = false) {

        for (int k = 0; k < gens; k++) {
            for (int i = 0; i < height - 1; i++) {
                for (int j = 0; j < width - 1; j++) {

                    if (halfplane) {
                        if ((i < ci) || ((i == ci) && (j <= cj))) {
                            // skip if we are lexicographically <= the centre cell:
                            continue;
                        }
                    }

                    // restrict to have low density:
                    ced.add_proposition(
                        {literals[i][j][k], literals[i+1][j][k], literals[i][j+1][k], literals[i+1][j+1][k]},
                        [](uint64_t x) {
                            return (__builtin_popcountll(x) <= 2);
                        }
                    );
                }
            }
        }
    }

    void impose_rules(bool periodic, bool onlygood) {

        int maxgen = periodic ? gens : gens - 1;
        for (int k = 0; k < maxgen; k++) {

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if ((i == 0) || (i == height - 1) || (j == 0) || (j == width - 1)) {
                        // phoenix condition:
                        ced.add_clause({-literals[i][j][k], -literals[i][j][(k+1) % gens]});
                        continue;
                    }

                    // construct a 10-element vector consisting of the next state
                    // and its nine neighbours in the current generation:
                    std::vector<int32_t> v;
                    v.push_back(literals[i][j][(k+1) % gens]);
                    for (int i2 = i-1; i2 <= i+1; i2++) {
                        for (int j2 = j-1; j2 <= j+1; j2++) {
                            v.push_back(literals[i2][j2][k]);
                        }
                    }

                    // impose all prime implicants:
                    ced.add_proposition(v, [&](uint64_t x) {

                        // GoL rules:
                        bool next_state = x & 1;
                        bool this_state = (x >> 5) & 1;
                        auto count = __builtin_popcountll(x & 990);
                        bool actual_next_state = (this_state) ? ((count == 2) || (count == 3)) : (count == 3);
                        bool valid = (next_state == actual_next_state);

                        // phoenix condition:
                        valid = valid && ((!this_state) || (!next_state));

                        // neighbourhood condition:
                        if (onlygood) { valid = valid && is_good[x >> 1]; }

                        return valid;
                    });
                }
            }
        }
    }

    bool is_contradiction() {

        bool ruled_out = true;

        std::cout << "Solving..." << std::endl;
        ced.multisolve([&](const std::vector<int32_t> &solution) {
            ruled_out = false;
            std::cout << "Counterexample:" << std::endl;
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (solution[i*width+j] > 0) {
                        std::cout << "*";
                    } else {
                        std::cout << ".";
                    }
                }
                std::cout << std::endl;
            }
        });

        return ruled_out;
    }

    bool heavy_box_search(int ck) {

        ced.add_proposition(
            {literals[ci][cj][ck], literals[ci+1][cj][ck], literals[ci][cj+1][ck], literals[ci+1][cj+1][ck]},
            [](uint64_t x) {
                return (__builtin_popcountll(x) >= 3);
            }
        );
        
        impose_sparsity(true);
        impose_rules(false, false);
        return is_contradiction();
    }

    bool nhood_search(int nhood, int ck) {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int sign = ((nhood >> (i*3+j)) & 1) * 2 - 1;
                ced.add_clause({sign * literals[ci + i - 1][cj + j - 1][ck]});
            }
        }

        impose_sparsity();
        impose_rules(false, false);
        return is_contradiction();
    }

    void finite_constraint() {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((i + j < ci + cj) || ((i + j == ci + cj) && (i < ci))) {
                    for (int k = 0; k < gens - 2; k++) {
                        ced.identify_vars(literals[i][j][k], literals[i][j][k+2]);
                    }
                }
            }
        }
    }

    bool phoenix_search() {

        // non-p2 cell: 
        ced.add_clause({ literals[ci][cj][2]});
        ced.add_clause({-literals[ci][cj][0]});

        finite_constraint();
        impose_rules(true, true);

        return is_contradiction();
    }

    template<typename Fn>
    void de_bruijn_search(Fn lambda) {

        finite_constraint();
        impose_rules(false, true);

        ced.multisolve([&](const std::vector<int32_t> &solution) {
            uint64_t v = 0;
            for (int i = 0; i < dbheight; i++) {
                for (int j = 0; j < dbwidth; j++) {
                    if (i + j < ((dbwidth + dbheight - 6) >> 1)) { continue; }
                    int k = i * dbwidth + j;
                    if (solution[k] > 0) {
                        v |= (1ull << k);
                    }
                }
            }
            lambda(v);
        });
    }
};

} // anonymous namespace
