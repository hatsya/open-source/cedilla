#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "solver.hpp"
#include "product.hpp"

namespace hh { namespace {

std::vector<int32_t> load_target_from_file(std::string filename) {

    std::vector<int32_t> target;
    std::ifstream iss(filename);
    std::string str;
    while (std::getline(iss, str)) {
        for (char c : str) {
            if (c == '0') {
                target.push_back(0);
            } else if (c == '+') {
                target.push_back(1);
            } else if (c == '-') {
                target.push_back(-1);
            }
        }
    }

    return target;
}

std::string sol2str(const std::vector<std::vector<std::vector<int32_t>>> &sol) {

    std::ostringstream ss;

    for (size_t i = 0; i < sol.size(); i++) {
        for (size_t j = 0; j < sol[i].size(); j++) {
            if (j) {
                ss << ',';
            } else if (i) {
                ss << ';';
            }
            for (size_t k = 0; k < sol[i][j].size(); k++) {
                int x = sol[i][j][k];
                if (x == -1) {
                    ss << '-';
                } else if (x == 0) {
                    ss << '0';
                } else if (x == 1) {
                    ss << '+';
                }
            }
        }
    }

    return ss.str();
}

template<typename Fn>
int tensorlift(const std::vector<int32_t> &target, const std::vector<std::vector<std::vector<int32_t>>> &f2sol, Fn lambda) {

    size_t rank = f2sol.size();
    size_t n_dims = f2sol[0].size();
    std::vector<size_t> dims(n_dims);
    for (size_t i = 0; i < n_dims; i++) { dims[i] = f2sol[0][i].size(); }

    for (size_t j = 0; j < rank; j++) {
        for (size_t i = 0; i < n_dims; i++) {
            if (f2sol[j][i].size() != dims[i]) {
                std::cerr << "Error: dimension " << i << " of tensor " << j << " is malformed (" << f2sol[j][i].size() << " != " << dims[i] << ")" << std::endl;
                return 1;
            }
        }
    }

    size_t total_size = 1;
    for (auto&& dim : dims) { total_size *= dim; }
    if (total_size != target.size()) {
        std::cerr << "Error: constituent tensors have size " << total_size << " but target has size " << target.size() << std::endl;
        return 1;
    }

    auto lits = f2sol;

    hh::cedilla ced;

    for (size_t i = 0; i < rank; i++) {
        for (size_t j = 0; j < n_dims; j++) {
            bool make_first_positive = (j > 0);
            for (size_t k = 0; k < dims[j]; k++) {
                lits[i][j][k] &= 1;
                if (lits[i][j][k]) {
                    int32_t lit = ced.new_literal(true);
                    lits[i][j][k] = lit;
                    if (make_first_positive) {
                        ced.identify_vars(lit, 1);
                        make_first_positive = false;
                    }
                }
            }
        }
    }

    bool already_failed = false;

    {
        size_t count = 0;
        hh::cartesian_product(dims, [&](const std::vector<size_t> &idx) {
            int target_val = target[count];
            std::vector<int32_t> plits;
            for (size_t i = 0; i < rank; i++) {
                bool is_zero = false;
                std::vector<int32_t> blits;
                for (size_t j = 0; j < n_dims; j++) {
                    int32_t lit = lits[i][j][idx[j]];
                    if (lit == 0) { is_zero = true; }
                    blits.push_back(lit);
                }
                if (is_zero) { continue; }
                int32_t lit = ced.new_literal();
                plits.push_back(lit);
                blits.push_back(lit);
                ced.flat_linear_clause(blits);
            }
            if (((int) (plits.size() & 1)) == (target_val & 1)) {
                ced.constrain_cardinality(plits, [&](int plus_ones, int minus_ones) {
                    return (plus_ones - minus_ones == target_val);
                });
            } else {
                std::cerr << "Error: claimed solution over F2 fails at point " << count << std::endl;
                already_failed = true;
            }
            count += 1;
        });
    }

    if (already_failed) {
        std::cerr << "Error: aborting due to failure over F2" << std::endl;
        return 1;
    }

    ced.multisolve([&](const std::vector<int32_t> &satsol) {

        auto res = lits;

        {
            size_t count = 0;
            for (size_t i = 0; i < rank; i++) {
                for (size_t j = 0; j < n_dims; j++) {
                    for (size_t k = 0; k < dims[j]; k++) {
                        if (res[i][j][k]) {
                            res[i][j][k] = (satsol[count++] < 0) ? 1 : -1;
                        }
                    }
                }
            }

            if (count != satsol.size()) {
                std::cerr << "Error: unexpected number of returned variables" << std::endl;
            }
        }

        // check that the solution actually works
        size_t count = 0;
        hh::cartesian_product(dims, [&](const std::vector<size_t> &idx) {
            int target_val = target[count];
            int sum_of_products = 0;
            for (size_t i = 0; i < rank; i++) {
                int this_product = 1;
                for (size_t j = 0; j < n_dims; j++) {
                    this_product *= res[i][j][idx[j]];
                }
                sum_of_products += this_product;
            }
            if (sum_of_products != target_val) {
                std::cerr << "Error: claimed solution over Z fails at point " << count << " (" << sum_of_products << " != " << target_val << ")" << std::endl;
                already_failed = true;
            }
            count += 1;
        });

        lambda(res);
    });

    if (already_failed) {
        std::cerr << "Error: aborting due to failure over Z" << std::endl;
        return 1;
    }

    return 0;
}

} }
