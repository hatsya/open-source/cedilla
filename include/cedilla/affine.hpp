#pragma once
#include <vector>
#include <utility>
#include <set>
#include <map>

#include "../../f2reduce/f2reduce.h"
#include "dsds.hpp"

namespace hh {

typedef std::pair<std::vector<int32_t>, int32_t> affine_clause;

namespace {

/**
 * This performs canonisation (removal of negative literals), simplification
 * (elimination of duplicate literals), and sorting into reverse order.
 */
affine_clause simplify_linear_clause(const std::vector<int32_t> &v, int32_t initial_parity = 1) {

    std::vector<int32_t> w = v;
    int32_t total_parity = initial_parity;

    for (size_t i = 0; i < w.size(); i++) {
        if (w[i] < 0) {
            w[i] *= -1;
            total_parity *= -1;
        }
    }

    std::sort(w.begin(), w.end());

    std::vector<int32_t> x;

    size_t j = w.size();

    while (j --> 0) { // j goes to zero
        if (w[j] > 1) {
            if (j && (w[j] == w[j-1])) {
                j -= 1;
            } else {
                x.push_back(w[j]);
            }
        }
    }

    return affine_clause(x, total_parity);
}

affine_clause xor_affine_clauses(const affine_clause &c1, const affine_clause &c2) {

    std::vector<int32_t> merged;

    size_t i = 0; size_t j = 0;

    while ((i < c1.first.size()) || (j < c2.first.size())) {
        if (i == c1.first.size()) {
            merged.push_back(c2.first[j++]);
        } else if (j == c2.first.size()) {
            merged.push_back(c1.first[i++]);
        } else if (c1.first[i] < c2.first[j]) {
            merged.push_back(c2.first[j++]);
        } else if (c1.first[i] > c2.first[j]) {
            merged.push_back(c1.first[i++]);
        } else {
            i++; j++;
        }
    }

    return affine_clause(merged, c1.second * c2.second);
}

std::map<uint64_t, std::vector<affine_clause>> extract_cores(const std::set<affine_clause> &source) {

    std::set<int32_t> encountered;
    std::vector<affine_clause> core;

    std::map<int32_t, affine_clause> definitions;
    for (const auto &x : source) {

        if (x.first.size() == 0) { continue; }

        int32_t lv = x.first[0];
        affine_clause y = x;
        bool is_constraint = encountered.count(lv);

        for (auto v : x.first) {
            encountered.insert(v);
            if (definitions.count(v)) {
                y = xor_affine_clauses(y, definitions[v]);
            }
        }

        if (is_constraint) {
            core.push_back(y);
        } else {
            definitions[lv] = y;
        }
    }

    for (auto it = definitions.begin(); it != definitions.end(); ++it) {
        encountered.erase(it->first);
    }

    std::map<int32_t, uint64_t> var2idx;

    uint64_t core_vars = 0;
    for (auto rit = encountered.rbegin(); rit != encountered.rend(); ++rit) {
        var2idx[*rit] = (core_vars++);
    }

    hh::dsds uf(core_vars);

    for (auto it = core.begin(); it != core.end(); ++it) {
        const std::vector<int32_t> &v = it->first;
        for (uint64_t i = 1; i < v.size(); i++) {
            uf.merge(var2idx[v[i-1]], var2idx[v[i]]);
        }
    }

    std::map<uint64_t, std::vector<affine_clause>> cores;

    for (auto it = core.begin(); it != core.end(); ++it) {
        cores[var2idx[it->first[0]]].push_back(*it);
    }

    return cores;
}


int32_t solve_single_core(const std::vector<affine_clause> &core, std::vector<affine_clause> &output) {

    uint64_t core_size = core.size();

    std::map<int32_t, uint64_t> var2idx;
    std::vector<int32_t> idx2var;

    std::set<int32_t> encountered;

    for (uint64_t i = 0; i < core.size(); i++) {
        for (uint64_t j = 0; j < core[i].first.size(); j++) {
            encountered.insert(core[i].first[j]);
        }
    }

    uint64_t core_vars = 0;
    for (auto rit = encountered.rbegin(); rit != encountered.rend(); ++rit) {
        idx2var.push_back(*rit);
        var2idx[*rit] = (core_vars++);
    }

    // std::cerr << "core_vars = " << core_vars << "; core_size = " << core_size << std::endl;

    uint64_t stride = get_recommended_stride(core_vars + 1);

    std::vector<uint64_t> matrix(core_size * stride);

    for (uint64_t row = 0; row < core_size; row++) {
        for (int32_t var : core[row].first) {
            uint64_t col = var2idx[var];
            matrix[row * stride + (col >> 6)] ^= (1ull << (col & 63));
        }
        if (core[row].second < 0) {
            matrix[row * stride + (core_vars >> 6)] ^= (1ull << (core_vars & 63));
        }
    }

    inplace_rref_strided(&(matrix[0]), core_size, core_vars + 1, stride);
    int32_t leadings = 0;

    for (uint64_t row = 0; row < core_size; row++) {
        std::vector<int32_t> new_clause;
        int32_t total_parity = 1;
        for (uint64_t col = 0; col <= core_vars; col++) {
            if (1 & (matrix[row * stride + (col >> 6)] >> (col & 63))) {
                if (col == core_vars) {
                    total_parity = -1;
                } else {
                    new_clause.push_back(idx2var[col]);
                }
            }
        }
        if (new_clause.size() == 0) {
            if (total_parity < 0) {
                return -1; // impossible
            }
        } else {
            output.emplace_back(new_clause, total_parity);
            leadings += 1;
        }
    }

    int32_t kdim = (((int32_t) core_vars) - leadings);
    return kdim;
}


int32_t linear_solve(const std::set<affine_clause> &source, std::vector<affine_clause> &output) {

    auto cores = extract_cores(source);

    int32_t total_kdim = 0;

    for (auto it = cores.begin(); it != cores.end(); ++it) {
        int32_t kdim = solve_single_core(it->second, output);
        if (kdim < 0) { return kdim; }
        total_kdim += kdim;
    }

    return total_kdim;
}


} // anonymous namespace

}
