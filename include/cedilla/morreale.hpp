#pragma once
#include <stdint.h>
#include <vector>
#include <algorithm>

namespace hh {
namespace {

/**
 * Algorithm from exercise 29 of Knuth 7.1.1
 */
template<typename Fn>
void buddy_scan(const uint32_t* v, size_t m, uint32_t j2, Fn lambda) {

    if (m == 0) { return; }

    size_t k = 0;
    size_t l = 0;

    while (k < m - 1) {
        if (v[k] & j2) { k++; continue; } // B2
        if (l <= k) { l = k + 1; } // B3
        while (v[l] < v[k] + j2) { l++; if (l >= m) { return; } } // B4
        if ((v[l] ^ v[k]) >= 2*j2) { k = l; continue; } // B5
        if (v[l] == v[k] + j2) { lambda(k, l); } // B6
        k++; // B7
    }
}

/**
 * Algorithm from exercise 30 of Knuth 7.1.1
 */
template<typename Fn>
void prime_implicants(const uint32_t* v, size_t m, int n, Fn lambda) {

    if (m == 0) { return; }

    std::vector<uint32_t> S; S.reserve(2*m + n);
    std::vector<uint32_t> T; T.reserve(2*m + n);

    // P1
    S.insert(S.end(), &(v[0]), &(v[m]));
    T.resize(m);
    std::sort(S.begin(), S.end());
    for (int j = 0; j < n; j++) {
        uint32_t j2 = (1u) << j;
        buddy_scan(&(S[0]), m, j2, [&](size_t k, size_t l) {
            T[k] |= j2;
            T[l] |= j2;
        });
    }
    {
        size_t dst = 0;
        for (size_t src = 0; src < m; src++) {
            if (T[src]) {
                T[dst] = T[src];
                S[dst] = S[src];
                dst++;
            } else {
                lambda(((uint32_t) 0), S[src]);
            }
        }
    }

    uint32_t A = 0;

    // we use an additional stack to store the list ends instead
    // of interleaving it with S:
    std::vector<size_t> U(1);

    for (;;) {
        // P2
        uint32_t j2 = 1;
        size_t lend = S.size();
        if (U.back() == lend) { j2 = (A & (-A)); }
        A += j2;
        while ((j2 & A) == 0) {
            j2 += j2; lend = U.back(); U.pop_back();
        }
        if (j2 >= (1u << n)) { return; }

        S.resize(lend); T.resize(lend);
        size_t lbegin = U.back();

        // P3
        buddy_scan(&(S[lbegin]), lend - lbegin, j2, [&](size_t k, size_t l) {
            uint32_t x = (T[lbegin + k] & T[lbegin + l]) ^ j2;
            if (x) {
                S.push_back(S[lbegin + k]);
                T.push_back(x);
            } else {
                lambda(A, S[lbegin + k]);
            }
        });
        U.push_back(lend);
    }
}

}
}
