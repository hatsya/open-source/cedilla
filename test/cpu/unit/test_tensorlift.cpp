#include <cedilla/tensorlift.hpp>
#include <gtest/gtest.h>

TEST(Tensorlift, StrassenSquared) {

    /*
    std::vector<std::vector<std::vector<int32_t>>> strassen{
        {{1, 0, 0, 1}, {1, 0, 0, 1}, {1, 0, 0, 1}},
        {{0, 0, 1, 1}, {1, 0, 0, 0}, {0, 0, 1, 1}},
        {{0, 1, 0, 1}, {0, 0, 1, 1}, {1, 0, 0, 0}},
        {{1, 0, 1, 0}, {1, 1, 0, 0}, {0, 0, 0, 1}},
        {{1, 1, 0, 0}, {0, 0, 0, 1}, {1, 1, 0, 0}},
        {{0, 0, 0, 1}, {1, 0, 1, 0}, {1, 0, 1, 0}},
        {{1, 0, 0, 0}, {0, 1, 0, 1}, {0, 1, 0, 1}}
    };

    std::vector<std::vector<std::vector<int32_t>>> strassen_squared;

    for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 7; j++) {
            strassen_squared.emplace_back(3);
            for (int k = 0; k < 3; k++) {
                for (int l = 0; l < 4; l++) {
                    for (int m = 0; m < 4; m++) {
                        strassen_squared[i*7+j][k].push_back(strassen[i][k][l] * strassen[j][k][m]);
                    }
                }
            }
        }
    }

    std::vector<int32_t> target;
    for (int i = 0; i < 16; i++) {
        for (int j = 0; j < 16; j++) {
            for (int k = 0; k < 16; k++) {
                int d = 0;
                for (int l = 0; l < 49; l++) {
                    d ^= strassen_squared[l][0][i] * strassen_squared[l][1][j] * strassen_squared[l][2][k];
                }
                target.push_back(d);
            }
        }
    }

    uint64_t count = 0;
    
    int res = hh::tensorlift(target, strassen_squared,
        [&](const std::vector<std::vector<std::vector<int32_t>>>&) {
        count += 1;
        std::cerr << "." << std::flush;
    });

    std::cerr << std::endl;

    EXPECT_EQ(res, 0);
    EXPECT_GT(count, 0);

    std::cerr << count << " solutions to Strassen^2 found." << std::endl;
    */
}

TEST(Tensorlift, Strassen) {

    size_t count = 0;

    int res = hh::tensorlift(
    {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 0,
        1, 0, 0, 0,
        0, 1, 0, 0,

        0, 0, 1, 0,
        0, 0, 0, 1,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    }, {
        {{1, 0, 0, 1}, {1, 0, 0, 1}, {1, 0, 0, 1}},
        {{0, 0, 1, 1}, {1, 0, 0, 0}, {0, 0, 1, 1}},
        {{0, 1, 0, 1}, {0, 0, 1, 1}, {1, 0, 0, 0}},
        {{1, 0, 1, 0}, {1, 1, 0, 0}, {0, 0, 0, 1}},
        {{1, 1, 0, 0}, {0, 0, 0, 1}, {1, 1, 0, 0}},
        {{0, 0, 0, 1}, {1, 0, 1, 0}, {1, 0, 1, 0}},
        {{1, 0, 0, 0}, {0, 1, 0, 1}, {0, 1, 0, 1}}
    }, [&](const std::vector<std::vector<std::vector<int32_t>>>&) {
        count += 1;
        std::cerr << "." << std::flush;
    });

    std::cerr << std::endl;

    EXPECT_EQ(res, 0);
    EXPECT_GT(count, 0);

    std::cerr << count << " solutions to Strassen found." << std::endl;

}
