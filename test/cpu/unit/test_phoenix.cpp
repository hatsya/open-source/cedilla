#include <gtest/gtest.h>
#include <cedilla/phoenix.hpp>

bool heavy_box_test(int height, int width, int gens, int kgen) {

    Phoenix p(height, width, gens);
    return p.heavy_box_search(kgen);

}

bool phoenix_test(int height, int width, int gens) {

    Phoenix p(height, width, gens);
    return p.phoenix_search();

}


TEST(Phoenix, HeavyBox) {

    EXPECT_EQ(heavy_box_test(16, 16, 8, 4), false);
    EXPECT_EQ(heavy_box_test(16, 16, 8, 5), true);

}


void nhood_test(int height, int width, int gens, int kgen, int nhood) {

    Phoenix p(height, width, gens);
    bool ruled_out = p.nhood_search(nhood, kgen);

    EXPECT_EQ(ruled_out, !(p.is_good[nhood]));

}

inline int transpose9(int x) {
    return (x & 0x111) | ((x & 0x22) << 2) | ((x & 0x88) >> 2) | ((x & 0x04) << 4) | ((x & 0x40) >> 4);
}
inline int vflip9(int x) {
    return (x & 0x38) | ((x & 0x07) << 6) | ((x & 0x1c0) >> 6);
}
inline int hflip9(int x) {
    return (x & 0x92) | ((x & 0x49) << 2) | ((x & 0x124) >> 2);
}



TEST(Phoenix, Neighbourhoods) {

    int nhoods = 0;

    for (int i = 0; i < 512; i++) {

        int mi = i;

        for (int k = 1; k < 8; k++) {
            int j = i;
            if (k & 1) { j = transpose9(j); }
            if (k & 2) { j = vflip9(j); }
            if (k & 4) { j = hflip9(j); }
            if (j < mi) { mi = j; }
        }

        if (mi < i) { continue; }

        nhoods += 1;
        nhood_test(15, 15, 8, 5, i);
    }

    EXPECT_EQ(nhoods, 102);
}


TEST(Phoenix, P4) {

    EXPECT_EQ(phoenix_test(23, 23, 4), false);
    EXPECT_EQ(phoenix_test(25, 25, 4), true);

}
