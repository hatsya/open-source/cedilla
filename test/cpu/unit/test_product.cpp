#include <gtest/gtest.h>
#include <cedilla/product.hpp>


void test_cartesian_product(const std::vector<uint32_t> &x) {

    uint32_t count = 0;

    std::vector<uint32_t> z;

    hh::cartesian_product(x, [&](const std::vector<uint32_t> &y) {
        if (count) { EXPECT_LT(z, y); }
        z = y;
        count++;
        EXPECT_EQ(x.size(), y.size());
        for (size_t i = 0; i < x.size(); i++) {
            EXPECT_LT(y[i], x[i]);
        }
    });

    uint32_t expected_count = 1;
    for (auto&& l : x) { expected_count *= l; }
    EXPECT_EQ(count, expected_count);
}


TEST(CartesianProduct, MultipleSizes) {
    test_cartesian_product({3, 4, 5});
    test_cartesian_product({4, 5, 6, 7});
    test_cartesian_product({0, 1000, 1000, 1000});
}
