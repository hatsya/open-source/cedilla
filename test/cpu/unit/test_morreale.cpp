#include <gtest/gtest.h>
#include <cedilla/solver.hpp>
#include <iostream>

TEST(Morreale, PrimeImplicants) {

    std::vector<uint32_t> x{3, 5, 6, 7, 9, 10, 11, 12, 13, 14};
    std::vector<std::pair<uint32_t, uint32_t>> y;

    hh::prime_implicants(&x[0], x.size(), 4, [&](uint32_t a, uint32_t b) {
        y.emplace_back(a, b);
    });

    EXPECT_EQ(y.size(), 12);

}

TEST(Morreale, SymmetricFunction) {

    hh::cedilla x;

    int32_t a = x.new_literal();
    int32_t b = x.new_literal();
    int32_t c = x.new_literal();
    int32_t d = x.new_literal();
    int32_t e = x.new_literal();

    x.add_proposition({a, b, c, d, e}, [](uint32_t x) {
        int weight = __builtin_popcount(x);
        return (weight >= 2) && (weight <= 4);
    });

    auto cnf = x.compile_cnf();

    for (auto&& m : cnf) {
        std::cout << m << " ";
    }

    std::cout << std::endl;

}
