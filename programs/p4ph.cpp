#include <chrono>
#include <cedilla/phoenix.hpp>


int main() {

    int i = 3;

    while (true) {

        Phoenix p(25, 25, i);
        auto before = std::chrono::high_resolution_clock::now();
        bool ruled_out = p.phoenix_search();
        auto after = std::chrono::high_resolution_clock::now();

        if (!ruled_out) { break; }

        double seconds = 1.0e-9 * std::chrono::duration_cast<std::chrono::nanoseconds>(after - before).count();

        std::cout << "period-" << i << " finite phoenices eliminated in " << seconds << " seconds." << std::endl;

        i += 1;

    }

    return 0;

}
