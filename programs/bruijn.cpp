#include <mutex>
#include <atomic>
#include <thread>
#include <unordered_set>
#include <cedilla/phoenix.hpp>


struct BruijnGraph {

    std::mutex mtx;
    std::vector<std::vector<int64_t>> vertices;
    std::unordered_set<int64_t> encountered;

    void try_append(int64_t src, int64_t dst) {
        std::unique_lock<std::mutex> lock(mtx);
        // we have acquired the mutex, so can safely write to stdout:
        std::cout << src << " -> " << dst << std::endl;
        if (encountered.count(src) == 0) {
            // vertex is new; add it to the list:
            vertices[vertices.size() - 1].push_back(src);
            encountered.insert(src);
        }
    }

    void try_print(int64_t dst, int tid, bool starting) {
        std::unique_lock<std::mutex> lock(mtx);
        std::cout << (starting ? "# starting node " : "# finishing node ") << dst << " on thread " << tid << std::endl;
    }

    void search(int64_t dst, int tid) {

        int width = 29;
        int gens = 32;
        int firstgen = 17;
        int nsize = 7;

        int nd = (dst >= 0) ? nsize : 1;
        int c = (width - nd) >> 1;

        Phoenix p(width, width, gens, firstgen, (width - nsize) >> 1, true);

        {
            // impose neighbourhood at generation firstgen+2 and ensure that it
            // is different at generation firstgen:
            std::vector<int32_t> v;
            for (int i = 0; i < nd; i++) {
                for (int j = 0; j < nd; j++) {
                    if (i + j < nd - 3) { continue; }
                    int sign = 2 * ((dst >> (nd*i+j)) & 1) - 1;
                    p.ced.add_clause({sign * p.literals[c+i][c+j][firstgen+2]});
                    v.push_back(     -sign * p.literals[c+i][c+j][firstgen] );
                }
            }
            p.ced.add_clause(v);
        }

        try_print(dst, tid, true);
        p.de_bruijn_search([&](int64_t src) {
            try_append(src, dst);
        });
        try_print(dst, tid, false);
    }

    void expand() {
        size_t oldsize = vertices.size();
        std::cout << "# Running layer " << oldsize << "..." << std::endl;
        vertices.resize(oldsize + 1);
    }

    void initialise() {
        expand();
        search(-1, 0);
    }

    void push_back(const std::vector<int64_t> &v) {
        vertices.push_back(v);
        for (const auto &x : v) { encountered.insert(x); }
    }
};


void run_worker(int tid, std::atomic<uint64_t> *ctr, BruijnGraph *bg, uint64_t n_tasks) {

    for (;;) {
        // dequeue subtask:
        uint64_t idx = (uint64_t) ((*ctr)++);
        if (idx >= n_tasks) { break; }
        volatile int64_t task = bg->vertices[bg->vertices.size() - 2][idx];
        bg->search(task, tid);
    }
}


int main(int argc, char* argv[]) {

    if (argc != 2) {
        std::cerr << "Usage: ./bruijn parallelism" << std::endl;
        return 1;
    }

    int parallelism = std::stoll(argv[1]);

    BruijnGraph bg;

    if (bg.vertices.empty()) { bg.initialise(); }

    for (;;) {
        uint64_t layer = bg.vertices.size() - 1;
        uint64_t n = bg.vertices[layer].size();
        std::cout << "# Layer " << layer << " complete with " << n << " vertices: {";

        for (uint64_t i = 0; i < n; i++) {
            if (i) { std::cout << ", "; }
            std::cout << bg.vertices[layer][i];
        }

        std::cout << "}" << std::endl;

        if (n == 0) { break; }

        bg.expand();

        std::atomic<uint64_t> ctr{0ull};
        std::vector<std::thread> workers;

        for (int i = 0; i < parallelism; i++) {
            workers.emplace_back(run_worker, i, &ctr, &bg, n);
        }

        for (auto&& w : workers) { w.join(); }
    }

    return 0;

}

