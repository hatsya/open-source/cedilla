#include <cedilla/tensorlift.hpp>
#include <stdio.h>
#include <atomic>
#include <algorithm>
#include <thread>

struct SolutionContainer {

    uint16_t x[152];

};

bool operator<(const SolutionContainer &lhs, const SolutionContainer &rhs) {

    // prefer higher-popcount candidates:
    return (lhs.x[148] > rhs.x[148]);

}

void run_worker(std::atomic<uint64_t> *ctr, uint64_t n_tasks, const SolutionContainer *problems, const std::vector<int32_t> *target) {

    for (;;) {
        // dequeue subtask:
        uint64_t idx = (uint64_t) ((*ctr)++);
        if (idx >= n_tasks) { break; }

        auto problem = problems[idx];

        if ((idx % 1000) == 0) {
            std::cerr << "Running subtask " << idx << ": orbit=" << problem.x[144] << "; popcount=" << problem.x[148] << std::endl;
        }

        std::vector<std::vector<std::vector<int32_t>>> candidate;

        for (int j = 0; j < 48; j++) {
            candidate.emplace_back(3);
            for (int k = 0; k < 3; k++) {
                uint16_t y = problem.x[j*3+k];
                for (int l = 0; l < 16; l++) {
                    candidate[j][k].push_back((y >> l) & 1);
                }
            }
        }

        int res = hh::tensorlift((*target), candidate, [&](const std::vector<std::vector<std::vector<int32_t>>> &sol) {
            std::cout << hh::sol2str(sol) << std::endl;
        });
    }

}



int main(int argc, char* argv[]) {

    if (argc != 5) {
        std::cerr << "Usage: at47 target.txt candidates.bin num_candidates parallelism" << std::endl;
        return 1;
    }

    std::vector<int32_t> target = hh::load_target_from_file(argv[1]);

    uint64_t num_candidates = std::stoll(argv[3]);
    uint64_t parallelism = std::stoll(argv[4]);

    std::vector<SolutionContainer> all_candidates(num_candidates);

    std::cerr << "Loading data..." << std::endl;
    {
        FILE* fptr = fopen(argv[2], "rb");
        fread(&(all_candidates[0]), 304, num_candidates, fptr);
        fclose(fptr);
    }

    std::cerr << "Sorting data..." << std::endl;
    std::sort(all_candidates.begin(), all_candidates.end());

    std::cerr << "Solving..." << std::endl;

    std::atomic<uint64_t> ctr{0};

    std::vector<std::thread> workers;

    for (size_t i = 0; i < parallelism; i++) {
        workers.emplace_back(run_worker, &ctr, num_candidates, &(all_candidates[0]), &target);
    }

    for (auto&& w : workers) {
        w.join();
    }

    return 0;
}
