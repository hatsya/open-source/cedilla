#include <cedilla/tensorlift.hpp>

int main(int argc, char* argv[]) {

    if (argc != 3) {
        std::cerr << "Usage: tensorlift target.txt candidates.txt" << std::endl;
        return 1;
    }

    std::vector<int32_t> target = hh::load_target_from_file(argv[1]);

    {
        std::ifstream iss(argv[2]);
        std::string str;
        while (std::getline(iss, str)) {

            std::vector<std::vector<std::vector<int32_t>>> candidate;
            candidate.emplace_back(1);

            for (char c : str) {
                if (c == ';') {
                    candidate.emplace_back(1);
                } else if (c == ',') {
                    candidate.back().emplace_back(0);
                } else if (c == '0') {
                    candidate.back().back().push_back(0);
                } else if (c == '1') {
                    candidate.back().back().push_back(1);
                }
            }

            int res = hh::tensorlift(target, candidate, [&](const std::vector<std::vector<std::vector<int32_t>>> &sol) {
                std::cout << hh::sol2str(sol) << std::endl;
            });

            if (res) { return res; }
        }
    }

    return 0;
}
